# Intro

This JavaScript full stack application used WebPack, Babel and React as Front-end. Use NodeJS runtime as Back-end. The Front-end gets information and make post call to Back-end and display results on page. To run both front-end and back-end is required. Please read below instructions how to serve the application locally.

Things can be improved:

1. Input validation on place inputs.
2. Input instruction should be cleared everytime user click on enter.
3. Project used React HookAPI but only used useState and useEffect. Project consumes state using localStorage. This area can be improved later.
4. Back-end project has unit testing coverage. Front-end is running out of time to write unit testing.

# server site

## Context

This nodejs server accept intial bus locations / direction and move / turn instructions. Returns bus location and direction after instruction steps.

## How to run:

```
node server/app.js
```

## API Doc:

- Endpoint /bus/drive
- POST
- Body: Object
- Body should include initalLocation object e.g { x: 0, y: 0, f: 'north' }
- Body should include instruction array e.g ['move', 'move', 'right', 'move', 'left'...]
- Instruction can support string right, left and move
- Return after move location and direction as object e.g { x: 4, y: 1, f: 'west' }

## Testing

```
npm run test
```

## Files:

app.js  
-lib
--bus.js
-service
--driver.js

## Technology requirement

Nodejs version 10.

# Client

## Context

Web application collect user's input and send quest to server. Response from server will displayed.

## Build

```
npm run build
```

## run locally http://localhost:8080

```
npm start
```
