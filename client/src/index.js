import 'babel-polyfill';
import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import 'index.scss';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

//components
import InitialInfoInput from './components/initalInfoInput';
import InstructionInfoInput from './components/InstructionInfoInput';
//lib
import request from './lib/http';

let BusBot = () => {
  let response = {};

  const [result, setResult] = useState({});

  const submitHandler = async () => {
    const initialInput = {
      x: Number(window.localStorage.getItem('x')),
      y: Number(window.localStorage.getItem('y')),
      f: window.localStorage.getItem('f')
    };

    const instructions = localStorage.getItem('instruction');

    try {
      response = await request(
        'http://localhost:3000/bus/drive',
        {},
        { initalLocation: initialInput, instruction: instructions.split(',') }
      );
      setResult(response);
    } catch (error) {}
  };

  useEffect(() => {
    setResult(response);
  }, []);

  return (
    <>
      <h1>Welcome to bus bot</h1>
      <InitialInfoInput />
      <InstructionInfoInput />

      <Button variant="contained" color="primary" onClick={submitHandler}>
        Ruote
      </Button>

      {result.x ? (
        <Paper elevation={1}>
          <Typography variant="h5" component="h3">
            Results:
          </Typography>
          <Typography component="p">{`${result.x}, ${result.y}, ${result.f}`}</Typography>
        </Paper>
      ) : null}
    </>
  );
};
ReactDOM.render(<BusBot />, document.getElementById('root'));
