import fetch from 'cross-fetch';

export default (host, headers, body) => {
  return new Promise((resolved, rej) => {
    fetch(host, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
      .then(res => {
        if (res.status >= 400) {
          throw new Error('Bad response from server');
        }
        return resolved(res.json());
      })
      .catch(err => {
        console.error(err);
        rej(err);
      });
  });
};
