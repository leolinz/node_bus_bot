import React, { useState, useEffect } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = {
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400
  },
  input: {
    marginLeft: 8,
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
};

const Inputs = props => {
  const { classes } = props;
  const [instructions, setInstructions] = useState([]);
  const [insValue, setInsValue] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const removeItem = index => {
    setInstructions(
      instructions.filter((res, i) => {
        return i !== index;
      })
    );
  };

  const submitHandler = () => {
    if (insValue !== 'move' && insValue !== 'left' && insValue !== 'right') {
      setErrorMessage('Instruction must use keyword right, left or move');
      return;
    }
    setErrorMessage('');
    instructions.push(insValue);
    setInsValue(instructions);
  };

  useEffect(() => {
    window.localStorage.setItem('instruction', instructions);
  });

  return (
    <div>
      <List subheader={<li />}>
        <li>
          <ul>
            {instructions.map((item, index) => (
              <ListItem key={`item-${index}`}>
                <DeleteIcon onClick={() => removeItem(index)} />
                <ListItemText primary={`${item}`}>
                  <DeleteIcon />
                </ListItemText>
              </ListItem>
            ))}
          </ul>
        </li>
      </List>

      <label>Input instruction</label>
      <Paper className={classes.root} elevation={1}>
        <InputBase
          type="text"
          className={classes.input}
          onChange={e => setInsValue(e.target.value)}
          placeholder="Type right, left or move"
        />
        <Divider className={classes.divider} />
        <IconButton
          onClick={e => submitHandler(insValue)}
          color="primary"
          className={classes.iconButton}
          aria-label="Directions"
        >
          <DirectionsIcon />
        </IconButton>
      </Paper>
      {errorMessage}
    </div>
  );
};

Inputs.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Inputs);
