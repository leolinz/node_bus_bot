import React, { useState, useEffect } from 'react';
import Input from '@material-ui/core/Input';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const state = {
  directions: 0
};

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  input: {
    margin: theme.spacing.unit
  }
});

const directions = [
  {
    value: 'north',
    label: 'North'
  },
  {
    value: 'south',
    label: 'South'
  },
  {
    value: 'west',
    label: 'West'
  },
  {
    value: 'east',
    label: 'East'
  }
];

const InputX = ({ classes }) => {
  const [x, setX] = useState(0);

  useEffect(() => {
    window.localStorage.setItem('x', x);
  });

  return (
    <Input
      placeholder="X Range: 0-4"
      className={classes.input}
      type="number"
      value={x}
      onChange={e => setX(e.target.value)}
      inputProps={{
        'aria-label': 'Description'
      }}
    />
  );
};

const InputY = ({ classes }) => {
  const [y, setY] = useState(0);
  useEffect(() => {
    window.localStorage.setItem('y', y);
  });
  return (
    <Input
      placeholder="Y Range: 0-4"
      className={classes.input}
      type="number"
      value={y}
      onChange={e => setY(e.target.value)}
      inputProps={{
        'aria-label': 'Description'
      }}
    />
  );
};

const InputF = ({ classes }) => {
  const [f, setF] = useState(' ');
  useEffect(() => {
    window.localStorage.setItem('f', f);
  });
  return (
    <TextField
      select
      label="Direction Select"
      className={classes.textField}
      value={f}
      onChange={e => setF(e.target.value)}
    >
      {directions.map(option => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
};

const Inputs = props => {
  const { classes } = props;

  return (
    <div>
      <Input
        defaultValue="Place"
        className={classes.input}
        disabled
        inputProps={{
          'aria-label': 'Description'
        }}
      />
      <InputX classes={classes} />
      <InputY classes={classes} />
      <InputF classes={classes} />
    </div>
  );
};

Inputs.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Inputs);
