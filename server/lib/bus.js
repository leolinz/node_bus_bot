class Bus {
  constructor() {
    this.directions = ['west', 'north', 'east', 'south'];
  }

  drive(busStates, step) {
    switch (busStates.f) {
      case 'north':
        if (busStates.x === 4) throw new Error('Bus is on north boundary');
        busStates.x = busStates.x + 1;
        break;
      case 'south':
        if (busStates.x === 0) throw new Error('Bus is on south boundary');
        busStates.x = busStates.x - 1;
        break;
      case 'west':
        if (busStates.y === 0) throw new Error('Bus is on west boundary');
        busStates.y = busStates.y - 1;
        break;
      case 'east':
        if (busStates.y === 4) throw new Error('Bus is on east boundary');
        busStates.y = busStates.y + 1;
        break;
      default:
        throw 'bus facing is incorrect';
    }
    return busStates;
  }

  turn(startDirection, trunTo) {
    if (trunTo != 'left' && trunTo != 'right') {
      throw 'Turn direction must be eigher left or right.';
    }

    if (trunTo === 'left') {
      trunTo = -1;
    } else {
      trunTo = 1;
    }

    const startDirectionNumber = this.directions.indexOf(startDirection);

    if (startDirectionNumber === -1) throw new TypeError('Inital direction must be one of west, north, east or south.');

    let outputDirection = null;

    // trun left
    if (trunTo < 0) {
      if (startDirectionNumber != 0) {
        outputDirection = startDirectionNumber + trunTo;
      } else {
        outputDirection = 4 + trunTo;
      }
      // trun right
    } else {
      if (startDirectionNumber === 3) {
        outputDirection = -1 + trunTo;
      } else {
        outputDirection = startDirectionNumber + trunTo;
      }
    }
    return this.directions[outputDirection];
  }
}

module.exports = Bus;
