const Bus = require('../lib/bus');

exports.driver = (busStates, instructions) => {
  const robotBus = new Bus();
  busStates.f = busStates.f;
  instructions.forEach(step => {
    step = step.toLowerCase();
    if (step != 'move') {
      busStates.f = robotBus.turn(busStates.f, step);
    } else if (step === 'move') {
      robotBus.drive(busStates, step);
    }
  });

  return busStates;
};
