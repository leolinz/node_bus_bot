const Bus = require('../lib/bus');
var assert = require('assert');
// happy case
describe('West turn left', function() {
  it('should turned to south', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('west', 'left'), 'south');
  });
});

describe('West turn right', function() {
  it('should turned to north', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('west', 'right'), 'north');
  });
});

describe('North turn right', function() {
  it('should turned to east', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('north', 'right'), 'east');
  });
});

describe('North turn left', function() {
  it('should turned to west', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('north', 'left'), 'west');
  });
});

describe('South turn right', function() {
  it('should turned to west', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('south', 'right'), 'west');
  });
});

describe('South turn left', function() {
  it('should turned to east', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('south', 'left'), 'east');
  });
});

describe('East turn left', function() {
  it('should turned to north', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('east', 'left'), 'north');
  });
});

describe('East turn right', function() {
  it('should turned to south', function() {
    const robotBus = new Bus();
    assert.equal(robotBus.turn('east', 'right'), 'south');
  });
});

// // un-happy case

describe('String turn right', function() {
  it('should throw error', function() {
    const robotBus = new Bus();
    assert.throws(
      () => robotBus.turn('string', 'left'),
      Error,
      /^typeError: Inital direction must be one of west, north, east or south.$/
    );
  });
});

describe('Number turn right', function() {
  it('should throw error', function() {
    const robotBus = new Bus();
    assert.throws(
      () => robotBus.trun(100, 'right'),
      Error,
      /^typeError: Inital direction must be one of west, north, east or south.$/
    );
  });
});

describe('West turn number', function() {
  it('should throw error', function() {
    const robotBus = new Bus();
    assert.throws(
      () => robotBus.trun('west', 1),
      Error,
      /^typeError: Inital direction must be one of west, north, east or south.$/
    );
  });
});

describe('West turn string', function() {
  it('should throw error', function() {
    const robotBus = new Bus();
    assert.throws(
      () => robotBus.trun('west', 'string'),
      Error,
      /^typeError: Inital direction must be one of west, north, east or south.$/
    );
  });
});
