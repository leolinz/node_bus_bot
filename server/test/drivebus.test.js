const Bus = require('../lib/bus');
var assert = require('assert');

let busStatus = {};
// bus move out of boundary case
// bus at north boundary and move north
describe('Move at north boundary and move north by 1', function() {
  it('Bus should thorw error', function() {
    const robotBus = new Bus();
    busStatus = {
      y: 4,
      x: 4,
      f: 'north'
    };
    assert.throws(() => robotBus.drive(busStatus, 'move'), Error);
  });
});
// bus at south boundary and south
describe('Move at south boundary and move south by 1', function() {
  it('Bus should thorw error', function() {
    const robotBus = new Bus();
    busStatus = {
      y: 4,
      x: 0,
      f: 'south'
    };
    assert.throws(() => robotBus.drive(busStatus, 'move'), Error);
  });
});
// bus at west boundary and west
// bus at east boundary and east

// bus head to north and move
describe('Move north by 1', function() {
  it('Bus should stop at x 3', function() {
    const robotBus = new Bus();
    busStatus = {
      y: 4,
      x: 2,
      f: 'north'
    };
    let location = robotBus.drive(busStatus, 'move');
    assert.equal(location.x, 3);
  });
});

// bus move with in boundary case
// bus at north boundary and south
// bus at south boundary and north
// bus at west boundary and east
// bus at east boundary and west
