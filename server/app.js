const fastify = require('fastify')({ logger: true });
fastify.use(require('cors')());
const { driver } = require('./service/driver');

// Declare a route
fastify.post('/bus/drive', async (request, reply) => {
  // let initalLocation = { x: 0, y: 0, f: null };
  // let instruction = [];

  return driver(request.body.initalLocation, request.body.instruction);
});

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000);
    fastify.log.info(`server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
